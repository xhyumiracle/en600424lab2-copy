my_sk_path = "/home/xhyu/.ssh/keyforNetSec/20164.1.47806.1/20164.1.47806.1.rsa"
my_cert_path = "/home/xhyu/.ssh/keyforNetSec/20164.1.47806.1/20164.1.47806.1.cert"
CA_cert_path = "/home/xhyu/.ssh/keyforNetSec/xhyu_rsa_signed.cert"

def getPrivateKeyForAddr(addr):
  # ignoring addr, always return the same thing
  with open(my_sk_path) as f:
    return f.read()

def getCertsForAddr(addr):
  chain = []
  with open(my_cert_path) as f:
    chain.append(f.read())
  with open(CA_cert_path) as f:
    chain.append(f.read())
  return chain