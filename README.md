# EN600424Lab2
## Installation
* `$ git clone git@bitbucket.org:xhyumiracle/en600424lab2.git`
* Please do include both `en600424lab2/Playground` and `en600424lab2/Playground/src` in ***PYTHONPATH***

## Introduction
* RIP version 3
* Passed all the test including all tests under err_rate=90
* Directory
    * EN400626Lab2/Playground/src/rip
* Main stack
    * RIPStackNew
* Relative files:
    * RIPBaseProtocol.py
    * RIPClientProtocolNew.py
    * RIPServerProtocolNew.py
    * RIPTransportNew.py
    * RIPStackNew.py
    * RIPConfig.py
    * RIPMessage.py
    * RIPCrypto.py
    * CertFactory.py
* Written under Ubuntu 16.04 64 bits
* Use state machine to reframe the rip stack
* Modified Playground files:
    * Timer.py
        * Change OneShotTimer into reusable timer
    * StateMachine.py
        * Commit logs about entering and exiting.

## Updated Log Since submission-5
* Move CertFactory.py from /src/rip to top level.