import random

from RIPBaseProtocol import RipBaseProtocol
from RIPConfig import MAX_SEQUENCE_NUMBER, MAX_NONCE_NUMBER, TIMEOUT_CLOSE
from RIPConfig import logger, signal
from RIPMessage import RipMessage
from playground.network.common.Timer import callLater
from playground.network.common.statemachine.StateMachine import StateMachine
from twisted.internet.error import  ConnectionDone
from twisted.internet.protocol import Protocol


class RipServerProtocol(RipBaseProtocol):
    sm = StateMachine('ripServerStateMachine')
    questioning = False
    def __init__(self):
        super(RipServerProtocol, self).__init__()
        logger.setAgent('server')
        self.buildStateMachine()
        self.sm.start('closed')

    '****************************************************'
    ' State Machine Functions                            '
    '****************************************************'

    def buildStateMachine(self):
        self.sm.addState('closed',
                         (signal.receive, 'closed'),
                         (signal.listen, 'listen'))
        self.sm.addState('listen',
                         (signal.receive, 'listen'),
                         (signal.rcv1st, 'snn-rcv'),
                         onEnter=self.onEnterListen)
        self.sm.addState('snn-rcv',
                         (signal.receive, 'snn-rcv'),
                         (signal.establish, 'established'),
                         onEnter=self.onEnterSnnRcv)
        self.sm.addState('established',
                         (signal.receive, 'established'),
                         (signal.rcv1st, 'questioning'),
                         (signal.rcvcls, 'cls-rcv'),
                         (signal.sntcls, 'cls-snt'),
                         onEnter=self.onEnterEstablished)
        self.sm.addState('questioning',
                         (signal.receive, 'questioning'),
                         (signal.retnml, 'established'),
                         onEnter=self.onEnterQuestioning)
        self.sm.addState('cls-rcv',
                         (signal.sntcls, 'closed'),
                         onEnter=self.onEnterCloseRcv)
        self.sm.addState('cls-snt',
                         (signal.rcvcls, 'closed'),
                         (signal.receive, 'cls-snt'),
                         onEnter=self.onEnterCloseSnt)

    def onEnterListen(self, sig, ripMessage):
        logger.log('sm', '', 'listen', sig)
        if sig == signal.listen:
            return
        elif sig == signal.receive:
            if self.hsCheck1stPacket(ripMessage):
                self.sm.signal(signal.rcv1st, None)
        else:
            logger.log('err', 'what? we got an unexpected signal:' + sig + ' which means you wrote an bug man!')

    def onEnterSnnRcv(self, sig, ripMessage):
        logger.log('sm', '', 'snn-rcv', sig)
        if sig == signal.rcv1st:
            self.hsSend2ndPacket()
            self.higherTransport.timerStart(True)
        elif sig == signal.receive:
            if self.hsCheck3rdPacket(ripMessage):
                self.sm.signal(signal.establish, None)
            elif self.hsCheck1stPacket(ripMessage):
                self.higherTransport.timerStop(True)
                self.hsSend2ndPacket()
                self.higherTransport.timerStart(True)
        else:
            logger.log('err', 'what? we got an unexpected signal:' + sig + ' which means you wrote an bug man!')

    def onEnterEstablished(self, sig, ripMessage):
        logger.log('sm', '', 'established', sig)
        if sig == signal.establish:
            logger.log('hs', 'handshake successfully established!')
            self.sessionId = hex(self.nonce)[2:] + hex(self.peerNonce)[2:]
            self.peerSessionId = hex(self.peerNonce)[2:] + hex(self.nonce)[2:]
            self.makeHigherConnection(self.higherTransport)
        elif sig == signal.receive:
            if self.receivePacket(ripMessage):
                pass
            else:
                if self.hsCheck1stPacket(ripMessage):
                    self.sm.signal(signal.rcv1st, None)
        elif sig == signal.retnml:
            self.sessionId = hex(self.nonce)[2:] + hex(self.peerNonce)[2:]
            self.peerSessionId = hex(self.peerNonce)[2:] + hex(self.nonce)[2:]

    def onEnterQuestioning(self, sig, ripMessage):
        logger.log('sm', '', 'questioning', sig)
        if sig == signal.rcv1st:
            self.hsSend2ndPacket()
        if sig == signal.receive:
            if self.hsCheck3rdPacket(ripMessage):
                self.sm.signal(signal.retnml, None)
            elif self.receivePacket(ripMessage):
                self.sm.signal(signal.retnml, None)
            elif self.hsCheck1stPacket(ripMessage):
                self.higherTransport.timerStop(True)
                self.hsSend2ndPacket()
                self.higherTransport.timerStart(True)
            else:
                # TODO: reset
                pass

    def onEnterCloseSnt(self, sig, ripMessage):
        if sig == signal.sntcls:
            pass
        elif sig == signal.receive:
            if self.receivePacket(ripMessage):
                self.timerFin.cancel()
                self.timerFin.run(TIMEOUT_CLOSE)
        pass

    def onEnterCloseRcv(self, sig, data):
        self.sendCloseAndReset(True, False, True)
        self.sm.signal(signal.sntcls, None)
        Protocol.connectionLost(self, reason=ConnectionDone)
        self.higherProtocol().connectionLost(ConnectionDone)
        self.higherProtocol().transport=None
        self.setHigherProtocol(None)
        pass

    '****************************************************'
    ' State Machine Functions End                        '
    '****************************************************'
    ' Handshake Functions                                '
    '****************************************************'

    def hsCheck1stPacket(self, ripMessage):
        logger.log('hs', 'step1')
        numNonce = 1
        if not self.verifyCerts(ripMessage, numNonce):
            return False
        if not self.verifyCommonName(ripMessage, numNonce):
            return False
        if not ripMessage.sequence_number_notification_flag:
            logger.log('err', 'SNN is False')
            return False

        self.peerCert = ripMessage.certificate[numNonce + 0]
        self.ackNum = ripMessage.sequence_number + 1
        self.peerNonce = int(ripMessage.certificate[0], 16)
        return True

    def hsSend2ndPacket(self):
        logger.log('hs', 'step2')

        self.seqNum = random.randint(0, MAX_SEQUENCE_NUMBER)
        self.nonce = random.randint(0, MAX_NONCE_NUMBER)
        certs = [hex(self.nonce)[2:], hex(self.peerNonce + 1)[2:]] + self.certs

        ripMessage = RipMessage()
        ripMessage.sequence_number = self.seqNum
        ripMessage.acknowledgement_number = self.ackNum
        ripMessage.certificate = certs
        ripMessage.sessionID = ""
        ripMessage.acknowledgement_flag = True
        ripMessage.close_flag = False
        ripMessage.sequence_number_notification_flag = True
        ripMessage.reset_flag = False
        ripMessage.data = ""
        ripMessage.OPTIONS = []
        ripMessage.signature = ""

        self.higherTransport.writeRipMessage(ripMessage)
        self.higherTransport.sndBufferForHs = ripMessage
        self.seqNum += 1

        return True

    def hsCheck3rdPacket(self, ripMessage):
        logger.log('hs', 'step3')

        if not ripMessage.acknowledgement_flag:
            logger.log('err', 'ack is False')
            return False
        if ripMessage.acknowledgement_number != self.seqNum:
            logger.log('err', 'ack (%d)!= self.seq(%d)' %(ripMessage.acknowledgement_number, self.seqNum))
            return False
        if ripMessage.sequence_number != self.ackNum:
            logger.log('err', 'seq (%d)!= self.ack(%d)' %(ripMessage.sequence_number, self.ackNum))
            return False
        if ripMessage.certificate[0] != hex(self.nonce + 1)[2:]:
            logger.log('err', 'verify Nonce2 failed')
            return False

        self.higherTransport.timerStop(True)
        self.ackNum = ripMessage.sequence_number + 1
        return True

    '****************************************************'
    ' Handshake Functions End                            '
    '****************************************************'

    def connectionMade(self):
        super(RipServerProtocol, self).connectionMade()
        self.sm.signal(signal.listen, None)

    def connectionLost(self, reason=ConnectionDone):
        super(RipServerProtocol, self).connectionLost(reason)
        pass

    def dataReceived(self, data):
        self.recBuffer += data
        try:
            ripMessage, byteUsed = RipMessage.Deserialize(self.recBuffer)
            self.recBuffer = self.recBuffer[byteUsed:]
        except Exception, e:
            return
        logger.log('rcv', ripMessage)
        self.sm.signal(signal.receive, ripMessage)
        self.recBuffer and callLater(0, self.dataReceived, '')

