import random

from RIPBaseProtocol import RipBaseProtocol
from RIPConfig import MAX_SEQUENCE_NUMBER, MAX_NONCE_NUMBER, TIMEOUT_HANDSHAKE, TIMEOUT_CLOSE
from RIPConfig import logger, signal
from RIPMessage import RipMessage
from playground.network.common.Timer import OneshotTimer, callLater
from playground.network.common.statemachine.StateMachine import StateMachine
from twisted.internet.error import  ConnectionDone
from twisted.internet.protocol import Protocol


class RipClientProtocol(RipBaseProtocol):
    sm = StateMachine('ripServerStateMachine')
    questioning = False
    lastSeqNum = 0
    timerMakeConn = None
    def __init__(self):
        super(RipClientProtocol, self).__init__()
        logger.setAgent('client')
        self.buildStateMachine()
        self.sm.start('closed')

    '****************************************************'
    ' State Machine Functions                            '
    '****************************************************'

    def buildStateMachine(self):
        self.sm.addState('closed',
                         (signal.receive, 'closed'),
                         (signal.open, 'snn-snt'))
        self.sm.addState('snn-snt',
                         (signal.receive, 'snn-snt'),
                         (signal.rcv2nd, 'snn-rcv'),
                         onEnter=self.onEnterSnnSnt,
                         onExit=self.onExitSnnSnt)
        self.sm.addState('snn-rcv',
                         (signal.receive, 'snn-rcv'),
                         (signal.establish, 'established'),
                         (signal.rcvcls, 'cls-rcv'),
                         (signal.sntcls, 'cls-snt'),
                         onEnter=self.onEnterSnnRcv)
        self.sm.addState('established',
                         (signal.receive, 'established'),
                         (signal.rcvcls, 'cls-rcv'),
                         (signal.sntcls, 'cls-snt'),
                         onEnter=self.onEnterEstablished)
        self.sm.addState('cls-rcv',
                         (signal.sntcls, 'closed'),
                         onEnter=self.onEnterCloseRcv)
        self.sm.addState('cls-snt',
                         (signal.rcvcls, 'closed'),
                         (signal.receive, 'cls-snt'),
                         onEnter=self.onEnterCloseSnt)

    def onEnterSnnSnt(self, sig, data):
        logger.log('sm', '', 'snn-snt', sig)
        if sig == signal.open:
            self.hsSend1stPacket()
        elif sig == signal.receive:
            if self.hsCheck2ndPacket(data):
                self.hsSend3rdPacket()
                self.sm.signal(signal.rcv2nd, None)
        else:
            logger.log('err', 'what? we got an unexpected signal:' + sig + ' which means you wrote an bug man!')

    def onExitSnnSnt(self, sig, data):
        pass

    def onEnterSnnRcv(self, sig, data):
        logger.log('sm', '', 'snn-rcv', sig)
        if sig == signal.receive:
            if self.receivePacket(data):
                self.sm.signal(signal.establish, data)
            elif self.hsCheck2ndPacket(data, True):
                self.higherTransport.timerStop(True)
                self.timerMakeConn.cancel()
                self.timerMakeConn.run(2 * TIMEOUT_HANDSHAKE)
                self.higherTransport.resend(True)
        elif sig == signal.rcv2nd:
            logger.log('hs', 'handshake successfully established!')
            self.sessionId = hex(self.nonce)[2:] + hex(self.peerNonce)[2:]
            self.peerSessionId = hex(self.peerNonce)[2:] + hex(self.nonce)[2:]
            self.timerMakeConn.run(2 * TIMEOUT_HANDSHAKE)
        else:
            logger.log('err', 'what? we got an unexpected signal:' + sig + ' which means you wrote an bug man!')

    def onEnterEstablished(self, sig, ripMessage):
        logger.log('sm', '', 'established', sig)
        if sig == signal.establish:
            pass
        elif sig == signal.receive:
            self.receivePacket(ripMessage)
        else:
            logger.log('err', 'what? we got an unexpected signal:' + sig + ' which means you wrote an bug man!')

    def onEnterCloseSnt(self, sig, ripMessage):
        if sig == signal.sntcls:
            pass
        elif sig == signal.receive:
            if self.receivePacket(ripMessage):
                self.timerFin.cancel()
                self.timerFin.run(TIMEOUT_CLOSE)
        pass

    def onEnterCloseRcv(self, sig, data):
        self.sendCloseAndReset(True, False, True)
        self.sm.signal(signal.sntcls, None)
        Protocol.connectionLost(self, reason=ConnectionDone)
        self.higherProtocol().connectionLost(ConnectionDone)
        self.higherProtocol().transport=None
        self.setHigherProtocol(None)
        pass

    '****************************************************'
    ' State Machine Functions End                        '
    '****************************************************'
    ' Handshake Functions                                '
    '****************************************************'

    def hsSend1stPacket(self):
        logger.log('hs', 'step1')

        self.seqNum = random.randint(0, MAX_SEQUENCE_NUMBER)
        self.nonce = random.randint(0, MAX_NONCE_NUMBER)
        certs = [hex(self.nonce)[2:]] + self.certs

        ripMessage = RipMessage()
        ripMessage.sequence_number = self.seqNum
        ripMessage.acknowledgement_number = 0
        ripMessage.certificate = certs
        ripMessage.sessionID = ""
        ripMessage.acknowledgement_flag = False
        ripMessage.close_flag = False
        ripMessage.sequence_number_notification_flag = True
        ripMessage.reset_flag = False
        ripMessage.data = ""
        ripMessage.OPTIONS = []
        ripMessage.signature = ""

        self.higherTransport.writeRipMessage(ripMessage)
        self.higherTransport.sndBufferForHs = ripMessage
        self.higherTransport.timerStart(True)
        self.lastSeqNum = self.seqNum
        self.seqNum += 1

        return True

    def hsCheck2ndPacket(self, ripMessage, lastCheck = False):
        logger.log('hs', 'step2')

        numNonce = 2
        if not self.verifyCerts(ripMessage, numNonce):
            return False
        if not self.verifyCommonName(ripMessage, numNonce):
            return False
        if not ripMessage.acknowledgement_flag:
            logger.log('err', 'ack is False')
            return False
        selfSeqNum = lastCheck and self.lastSeqNum or self.seqNum
        if ripMessage.acknowledgement_number != selfSeqNum:
            logger.log('err', 'ack (%d)!= self.seq(%d)' %(ripMessage.acknowledgement_number, selfSeqNum))
            return False
        if not ripMessage.sequence_number_notification_flag:
            logger.log('err', 'SNN is False')
            return False
        if ripMessage.certificate[1] != hex(self.nonce + 1)[2:]:
            logger.log('err', 'verify Nonce1 failed')
            return False

        self.higherTransport.timerStop(True)
        self.peerCert = ripMessage.certificate[numNonce + 0]
        self.lastAckNum = self.ackNum
        self.ackNum = ripMessage.sequence_number + 1
        self.peerNonce = int(ripMessage.certificate[0], 16)

        return True

    def hsSend3rdPacket(self):
        logger.log('hs', 'step3')

        ripMessage = RipMessage()
        ripMessage.sequence_number = self.seqNum
        ripMessage.acknowledgement_number = self.ackNum
        ripMessage.certificate = [hex(self.peerNonce + 1)[2:]] + self.certs
        ripMessage.sessionID = ""
        ripMessage.acknowledgement_flag = True
        ripMessage.close_flag = False
        ripMessage.sequence_number_notification_flag = False
        ripMessage.reset_flag = False
        ripMessage.data = ""
        ripMessage.OPTIONS = []
        ripMessage.signature = ""

        self.higherTransport.writeRipMessage(ripMessage)
        self.higherTransport.sndBufferForHs = ripMessage
        self.lastSeqNum = self.seqNum
        self.seqNum += 1

        return True

    '****************************************************'
    ' Handshake Functions End                            '
    '****************************************************'
    # ' Protocol Stuff                                     '
    # '****************************************************'

    def connectionMade(self):
        super(RipClientProtocol, self).connectionMade()
        self.timerMakeConn = OneshotTimer(self.makeHigherConnection, self.higherTransport)
        self.sm.signal(signal.open, None)

    def connectionLost(self, reason=ConnectionDone):
        super(RipClientProtocol, self).connectionLost(reason)
        pass

    def dataReceived(self, data):
        self.recBuffer += data
        try:
            ripMessage, byteUsed = RipMessage.Deserialize(self.recBuffer)
            self.recBuffer = self.recBuffer[byteUsed:]
        except Exception, e:
            return
        logger.log('rcv', ripMessage)
        self.sm.signal(signal.receive, ripMessage)
        self.recBuffer and callLater(0, self.dataReceived, '')
