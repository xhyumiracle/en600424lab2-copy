import CertFactory
from twisted.internet.protocol import Protocol
from playground.network.common.Protocol import StackingProtocolMixin
from RIPConfig import HANDSHAKE_STEP0, HANDSHAKE_STEP1, DATA_TRANSPORT, QUESTIONING, DEBUG, STRICT
from RIPConfig import MAX_SEQUENCE_NUMBER, MAX_NONCE_NUMBER, MAX_SEGMENT_SIZE
from RIPConfig import logger
import random
from RIPCrypto import RipCrypto
from RIPTransportSimple import RipTransport
from RIPMessage import RipMessage
from RIPConfig import RECEIVER_WINDOW_SIZE, SENDER_WINDOW_SIZE, MAX_ACK_TOBE_SENT_SIZE, ACK_SEND_TIMEOUT
from playground.network.common.Timer import OneshotTimer, callLater
from twisted.internet.error import  ConnectionDone

class RipServerProtocol(StackingProtocolMixin, Protocol):
    def __init__(self):
        if DEBUG:
            print "rip protocol init"
            print "logprefix", self.logPrefix()
        self.__buffer = ""
        self.__status=HANDSHAKE_STEP0
        self.get_config()
        self.__crypto = RipCrypto(self.__mySkBytes)
        self.__ack_num = 0
        self.__ACK = False
        self.__FIN = False
        self.__RST = False
        self.__session_id = ""
        self.__peer_session_id = ""
        self.__Nonce1 = 0
        self.__Nonce2 = 0
        self.__snd_window_cur_size = 0
        self.__snd_window_cur_pos = 0
        self.__rec_window_cur_pos = 0
        self.__ack_to_be_sent_queue = []
        self.__ack_set = set()
        self.__map_seq_expected_ack = {}
        self.__map_ack_origined_seq = {}
        self.__receiver_buffer = {}
        '***********************************************************************'
        'Ack Timer Stuff                                                        '
        '***********************************************************************'
        self.__timer = []
        self.__map_ack_timer = {}
        self.__map_timer_ack = {}
        self.__unused_timer=[]
        self.__build_timers(MAX_ACK_TOBE_SENT_SIZE)

    def __build_timers(self, num):
        for i in range(num):
            self.__timer.append(OneshotTimer(self.__timeout, i))
        self.__unused_timer = range(num)

    def __get_an_unused_timer(self):
        if self.__unused_timer.__len__() == 0:
            logger.log('err', 'no free timer!!!!!!!!')
            self.reset_connection()
        ind = self.__unused_timer.pop(0)
        return ind

    def __return_an_unused_timer(self, ind, ack):
        self.__unused_timer.append(ind)
        self.__map_ack_timer.pop(ack)
        self.__map_timer_ack.pop(ind)
        return

    def __timeout(self, timer_ind):
        ack = self.__map_timer_ack[timer_ind]
        if DEBUG:
            print 'ack:', ack
        self.__remove_ack_to_be_sent_queue(ack)
        self.__return_an_unused_timer(timer_ind, ack)
        self.__send_ack_only(ack)
        logger.log('info', 'send ack back by timeout: ' + str(ack))

    def __timer_start(self, ack):
        timer_ind = self.__get_an_unused_timer()
        self.__map_ack_timer[ack] = timer_ind
        self.__map_timer_ack[timer_ind] = ack
        self.__timer[timer_ind].run(ACK_SEND_TIMEOUT)

    def __timer_stop(self, ack):
        timer_ind = self.__map_ack_timer[ack]
        self.__timer[timer_ind].cancel()
        self.__return_an_unused_timer(timer_ind, ack)

    def __send_ack_only(self, ack):
        self.__write("", True, self.get_seq_num(), ack, [], self.__session_id, True)

    def __remove_ack_to_be_sent_queue(self, ack):
        ind = self.__ack_to_be_sent_queue.index(ack)
        self.__ack_to_be_sent_queue.pop(ind)

    def reset_connection(self):
        pass

    '***********************************************************************'
    'Ack Timer Stuff End                                                    '
    '***********************************************************************'

    def get_seq_num(self):
        return self.__seq_num
    def get_ack_num(self):
        if self.__ack_to_be_sent_queue.__len__() == 0:
            # ACK, ack_num
            return False, 0
        else:
            ack = self.__ack_to_be_sent_queue.pop(0)
            self.__timer_stop(ack)
            return True, ack
        # return self.__ack_num
    def get_session_id(self):
        return self.__session_id
    def get_ACK(self):
        return self.__ACK
    def get_FIN(self):
        return self.__FIN
    def get_RST(self):
        return self.__RST
    def get_status(self):
        return self.__status
    def get_window_cur_size(self):
        return self.__snd_window_cur_size
    def add_window_cur_size(self, value):
        self.__snd_window_cur_size += value
    def get_window_cur_pos(self):
        return self.__snd_window_cur_pos
    def add_window_cur_pos(self, value):
        self.__snd_window_cur_pos += value
    def calc_expected_ack(self, ripMessage, is_send = False):
        seq = ripMessage.sequence_number
        len_data = len(ripMessage.data)
        if is_send:
            if len_data == 0 and ripMessage.acknowledgement_flag:
                if self.__status in [DATA_TRANSPORT]:
                    len_data = 0
                else:
                    len_data = 1
        else:
            if len_data == 0:
                if self.__status in [DATA_TRANSPORT, HANDSHAKE_STEP1] and ripMessage.acknowledgement_flag:
                    len_data = 0
                else:
                    len_data = 1
        ack = seq + len_data
        return ack
    def add_map_seq_expected_ack(self, ripMessage):
        seq = ripMessage.sequence_number
        ack = self.calc_expected_ack(ripMessage)
        self.__map_seq_expected_ack[seq] = ack
        self.__map_ack_origined_seq[ack] = seq
    def sign(self, data):
        return self.__crypto.sign(data)

    def inc_seq_num(self, value):
        logger.log('info', 'inc_seq_num, before: '+str(self.__seq_num))
        self.__seq_num += value
        logger.log('info', 'inc_seq_num, after: '+str(self.__seq_num))

    def get_config(self):
        # self.__common_name_prefix = "20164.1"
        # self.__common_name_group = "47806"
        # self.__common_name_my = "1"
        # self.my_sk_path = "/home/xhyu/.ssh/keyforNetSec/20164.1.47806.1/20164.1.47806.1.rsa"
        # self.my_cert_path = "/home/xhyu/.ssh/keyforNetSec/20164.1.47806.1/20164.1.47806.1.cert"
        # self.CA_cert_path = "/home/xhyu/.ssh/keyforNetSec/xhyu_rsa_signed.cert"
        # self.__get_my_cert()
        # self.__get_CA_cert()
        # self.__get_sk()
        # self.__certs = [self.__myCertBytes, self.__CACertBytes]
        self.__mySkBytes = CertFactory.getPrivateKeyForAddr("20164.1.100.1")
        self.__certs = CertFactory.getCertsForAddr("20164.1.100.1")

    def __get_my_cert(self):
        with open(self.my_cert_path) as f:
            certBytes = f.read()
        self.__myCertBytes = certBytes
        # return certBytes, ["123", "234"]

    def __get_CA_cert(self):
        with open(self.CA_cert_path) as f:
            CACertBytes = f.read()
        self.__CACertBytes = CACertBytes

    def __get_sk(self):
        with open(self.my_sk_path) as f:
            skBytes = f.read()
        self.__mySkBytes = skBytes

    def connectionMade(self):
        logger.log('con')
        self.__higherTransport = RipTransport(self.transport, self)

    def __write(self, data="", is_customize=False,  seq_num=0, ack_num=0, certs=[], session_id="", ACK=False, FIN=False, SNN=False, RST=False, options=[]):
        if is_customize:
            ripMessage = RipMessage()
            ripMessage.sequence_number = seq_num
            ripMessage.acknowledgement_number = ack_num
            ripMessage.certificate = certs
            ripMessage.sessionID = session_id
            ripMessage.acknowledgement_flag = ACK
            ripMessage.close_flag = FIN
            ripMessage.sequence_number_notification_flag = SNN
            ripMessage.reset_flag = RST
            ripMessage.OPTIONS = options
            ripMessage.signature = ""
            sig_data = ripMessage.__serialize__()
            sig = self.__sign(sig_data)
            ripMessage.signature = sig
            self.__higherTransport.write_ripMessage(ripMessage)
        else:
            self.__higherTransport.write(data)

    def __verify_certs(self, ripMessage, ind_cert):
        # TODO: verify the intermediate cert by root's public key
        if DEBUG:
            print 'verifying certs'
        # ripMessage.certificate
        certs = ripMessage.certificate
        if len(certs) < ind_cert+2:
            return False
        if not self.__crypto.verifyCertChain(certs[ind_cert + 1], certs[ind_cert + 0]):
            logger.log('err', 'cert chain verify failed')
            return False
        return True

    def __verify_common_name(self, ripMessage, ind_cert):
        certs = ripMessage.certificate
        if DEBUG:
            # TODO: here
            logger.log('info', "common name form cert: "+self.__crypto.getCommonName(certs[ind_cert + 0]))
        return True

    def __verify_signature(self, ripMessage):
        certBytes = self.__peer_cert
        signature = ripMessage.signature
        ripMessage.signature = ""
        data = ripMessage.__serialize__()
        if not self.__crypto.verifySignature(certBytes, data, signature):
            logger.log('err', 'signature verify failed')
            return False
        return True

    def __is_set_ack_flag(self, ripMessage):
        if not ripMessage.acknowledgement_flag:
            logger.log('info', 'ACK flag is False')
            return False
        logger.log('info', 'ACK flag is True')
        return True

    def __verify_ack_num_in_handshake(self, ripMessage):
        if self.__seq_num != ripMessage.acknowledgement_number:
            logger.log('err', 'verify ack num failed')
            return False
        return True

    def __is_in_sender_window(self, num):
        if DEBUG:
            print "is in sender window, num:", num," cur_pos:", self.__snd_window_cur_pos
            print "is in sender window,", self.__snd_window_cur_size
        return self.__snd_window_cur_pos <= num and num <= self.__snd_window_cur_pos + self.__snd_window_cur_size

    def __move_sender_window(self):
        while True:
            logger.log('info', 'mapseqexpected:'+str(self.__map_seq_expected_ack))
            logger.log('info', 'sndwindowcurpos:'+str(self.__snd_window_cur_pos))
            if not self.__map_seq_expected_ack.has_key(self.__snd_window_cur_pos):
                return
            e_ack = self.__map_seq_expected_ack[self.__snd_window_cur_pos]
            if e_ack in self.__ack_set:
                self.__snd_window_cur_size -= (e_ack - self.__snd_window_cur_pos)
                self.__ack_set.remove(e_ack)
                self.__map_seq_expected_ack.pop(self.__snd_window_cur_pos)
                # TODO: where to pop?
                # self.__map_ack_origined_seq.pop(e_ack)
                self.__snd_window_cur_pos = e_ack
            else:
                break

    def __add_to_ack_set(self, ack):
        self.__ack_set.add(ack)

    def __handle_ack_num(self, ripMessage):
        ack = ripMessage.acknowledgement_number
        if not self.__is_in_sender_window(ack):
            # logger.log('info', 'ack isn\'t in sender window')
            logger.log('err', 'ack isn\'t in sender window')
            return False
        origin_seq = self.__map_ack_origined_seq[ack]
        self.__higherTransport.seq_that_rec_ack(origin_seq)
        self.__add_to_ack_set(ack)
        self.__move_sender_window()
        return True

    def __is_smaller_than_receiver_window(self, seq):
        return self.__rec_window_cur_pos > seq

    def __is_bigger_than_receiver_window(self, seq):
        return seq >= self.__rec_window_cur_pos + RECEIVER_WINDOW_SIZE

    def __add_ack_to_be_send_queue(self, ripMessage):
        ack = self.calc_expected_ack(ripMessage)
        if ack in self.__ack_to_be_sent_queue:
            return
        if self.__ack_to_be_sent_queue.__len__() + 1 > MAX_ACK_TOBE_SENT_SIZE:
            # simply not send ack back if the queue is full
            logger.log('info', 'ack to be sent queue is full, will not sent this ack')
            return
        self.__ack_to_be_sent_queue.append(ack)
        self.__timer_start(ack)

    def __add_receiver_buffer(self, ripMessage):
        self.__receiver_buffer[ripMessage.sequence_number] = ripMessage

    def __move_receiver_window(self):
        while True:
            if self.__receiver_buffer.has_key(self.__rec_window_cur_pos):
                bufrip = self.__receiver_buffer.pop(self.__rec_window_cur_pos)
                self.__rec_window_cur_pos = self.calc_expected_ack(bufrip)
                if self.__status == DATA_TRANSPORT:
                    self.higherProtocol() and self.higherProtocol().dataReceived(bufrip.data)
            else:
                return

    def __is_in_buffer(self, ripMessage):
        return self.__receiver_buffer.has_key(ripMessage.sequence_number)

    def __handle_seq_num(self, ripMessage):
        seq = ripMessage.sequence_number
        if self.__is_bigger_than_receiver_window(seq):
            return False
        self.__add_ack_to_be_send_queue(ripMessage)
        if self.__is_smaller_than_receiver_window(seq) or self.__is_in_buffer(ripMessage):
            return False
        self.__add_receiver_buffer(ripMessage)
        self.__move_receiver_window()

    def __is_set_SNN(self, ripMessage):
        if not ripMessage.sequence_number_notification_flag:
            logger.log('info', 'SNN is False')
            return False
        return True

    def __verify_Nonce2(self, ripMessage):
        signed_nonce2 = ripMessage.certificate[0]
        certBytes = self.__peer_cert
        if not self.__crypto.verifySignature(certBytes, hex(self.__Nonce2+1)[2:], signed_nonce2):
            logger.log('err', 'verify Nonce2 failed')
            return False
        return True

    def __sign(self, data):
        return self.__crypto.sign(data)

    def __init_snd_window_cur_pos(self, value):
        self.__snd_window_cur_pos = value

    def __init_rec_window_cur_pos(self, value):
        self.__rec_window_cur_pos = value

    def __check_1st_packet(self, ripMessage):
        num_nonce = 1
        logger.log('hs', 'step1')
        if not self.__verify_certs(ripMessage, num_nonce):
            return False
        if not self.__verify_common_name(ripMessage, num_nonce):
            return False
        self.__peer_cert = ripMessage.certificate[num_nonce + 0]
        # if not self.__verify_signature(ripMessage):
        #     return False
        if STRICT and not self.__is_set_SNN(ripMessage):
            return False
        self.__init_rec_window_cur_pos(ripMessage.sequence_number)
        self.__handle_event('rec_SEQ', ripMessage)
        self.__Nonce1 = int(ripMessage.certificate[0], 16)
        return True

    def __send_2nd_packet(self, resend):
        logger.log('hs', 'step2')
        if resend:
            self.__init_snd_window_cur_pos(self.__2nd_seq_num)
            ACK, ack_num = self.get_ack_num()
            self.__higherTransport.resend_not_timeout_packet(self.__2nd_seq_num)
            return
        self.__seq_num = random.randint(0, MAX_SEQUENCE_NUMBER)
        self.__2nd_seq_num = self.__seq_num
        if DEBUG:
            self.__seq_num = 100
        self.__Nonce2 = random.randint(0, MAX_NONCE_NUMBER)
        signed_nonce1 = self.__sign(hex(self.__Nonce1 + 1)[2:])
        certs = [hex(self.__Nonce2)[2:], signed_nonce1] + self.__certs
        self.__init_snd_window_cur_pos(self.__seq_num)
        ACK, ack_num = self.get_ack_num()
        self.__write("", True, self.__seq_num, ack_num, certs, 0, ACK, False, True, False, [])

    def __check_3rd_packet(self, ripMessage):
        logger.log('hs', 'step3')
        if not self.__verify_signature(ripMessage):
            return False
        if not self.__is_set_ack_flag(ripMessage):
            return False
        # if not self.__verify_ack_num_in_handshake(ripMessage):
        if not self.__handle_event('rec_ACK', ripMessage):
            return False
        if not self.__verify_Nonce2(ripMessage):
            return False
        if not self.__verify_seq_num(ripMessage):
            return False
        self.__ack_num = ripMessage.sequence_number + 1
        return True

    def __handshake(self, ripMessage=""):
        if self.__status in [HANDSHAKE_STEP0, DATA_TRANSPORT]:
            success = self.__check_1st_packet(ripMessage)
            if not success:
                logger.log('err', "check 1st failed, data:" + str(ripMessage))
                # TODO: how to handle failure?
                self.__status = HANDSHAKE_STEP0
                return False
            self.__send_2nd_packet(False)
            self.__status = HANDSHAKE_STEP1
        elif self.__status == HANDSHAKE_STEP1:
            success = self.__check_3rd_packet(ripMessage)
            if not success:
                logger.log('err', "check 3rd failed, data:" + str(ripMessage))
                success = self.__check_1st_packet(ripMessage)
                if not success:
                    logger.log('err', "then check it as 1st also failed.")
                    return False
                # resend = True
                self.__send_2nd_packet(True)
                return True
            self.__status = DATA_TRANSPORT
            self.__handle_event('handshake_finish', object)
            # handshake success
            self.makeHigherConnection(self.__higherTransport)
        return True

    def __verify_sessionID(self, ripMessage):
        if ripMessage.sessionID != self.__peer_session_id:
            logger.log('err', 'verify sessionID failed')
            return False
        return True

    def __verify_seq_num(self, ripMessage):
        e_ack = self.__rec_window_cur_pos
        if ripMessage.sequence_number != e_ack:
            logger.log('err', 'seq number wrong, seq num: ' + str(ripMessage.sequence_number) + ' ack num:' + str(e_ack))
            return False
        return True

    def __is_set_reset_flag(self, ripMessage):
        return ripMessage.reset_flag

    def __is_set_close_flag(self, ripMessage):
        return ripMessage.close_flag

    def __need_handle_seq(self, ripMessage):
        if self.__status in [DATA_TRANSPORT, HANDSHAKE_STEP1] and ripMessage.data == "":
            return False
        return True

    def __handle_event(self, event, ripMessage):
        if event == 'rec_RST':
            pass
        elif event == 'rec_FIN':
            print "reach here, FIN"
            self.__higherTransport.loseConnection()
        elif event == 'rec_ACK':
            return self.__handle_ack_num(ripMessage)
            # self.__ACK = False # what's this ?
        elif event == 'rec_SEQ':
            return self.__handle_seq_num(ripMessage)
            # self.__ACK = True
        elif event == 'rec_rip':
            if not self.__verify_signature(ripMessage):
                return False
            if not self.__verify_sessionID(ripMessage):
                return False
            if self.__is_set_reset_flag(ripMessage):
                self.__handle_event('rec_RST', ripMessage)
                return False
            if self.__is_set_close_flag(ripMessage):
                self.__handle_event('rec_FIN', ripMessage)
            # first rec ACK then rec SEQ
            if self.__is_set_ack_flag(ripMessage):
                if not self.__handle_event('rec_ACK', ripMessage):
                    return False
            logger.log('info', 'handle seq')
            if self.__need_handle_seq(ripMessage):
                if not self.__handle_event('rec_SEQ', ripMessage):
                    return True
        elif event == 'handshake_finish':
            self.__session_id = hex(self.__Nonce2)[2:] + hex(self.__Nonce1)[2:]
            self.__peer_session_id = hex(self.__Nonce1)[2:] + hex(self.__Nonce2)[2:]
            logger.log('hs', 'success!')
            if DEBUG:
                print 'self.nonce1:', hex(self.__Nonce1)
                print 'self.nonce2:', hex(self.__Nonce2)
                print 'self.session_id:', self.__session_id
        else:
            logger.log('err', 'wrong event, seems you got a bug man, event: ' + event)
        return True

    def dataReceived(self, data):
        self.__buffer += data
        try:
            ripMessage, bytesUsed = RipMessage.Deserialize(self.__buffer)
            self.__buffer = self.__buffer[bytesUsed:]
        except Exception, e:
            # print "We had a deserialization error", e
            return

        logger.log('rcv', ripMessage)
        if self.__status != DATA_TRANSPORT:
            success = self.__handshake(ripMessage)
            if not success:
                # TODO: lose connection
                logger.log('err', 'handshake failed')
                # self.__status = HANDSHAKE_STEP0
                return
        else:
            self.higherProtocol() and self.higherProtocol().dataReceived(ripMessage.data)
        self.__buffer and callLater(0, self.dataReceived, "")

    def __sendMessage(self, data):
        self.__higherTransport.write(data)

    def connectionLost(self, reason=ConnectionDone):
        self.__higherTransport.loseConnection()
